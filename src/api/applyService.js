import request from '@/utils/request'

// 获取客户信息
export function fetchCustomerInfo(data) {
  return request({
    url: '/order/applyService/getCustomerInfo',
    method: 'post',
    data
  })
}

// 提交就医申请
export function submitApplyInfo(data) {
  return request({
    url: '/order/applyService/getApplyInfo',
    method: 'post',
    data
  })
}

// 获取就医申请详情
export function fetchServiceDetail(data) {
  return request({
    url: '/order/applyService/getServiceDetailInfo',
    method: 'post',
    data
  })
}

// 提交评价
export function submitEvaluate(data) {
  return request({
    url: '/order/applyService/submitEvaluate',
    method: 'post',
    data
  })
}

// 获取服务类型
export function fetchServiceList(data) {
  return request({
    url: '/order/applyService/getServiceInfo',
    method: 'post',
    data
  })
}

// 获取申请记录
export function fetchRecordList(data) {
  return request({
    url: '/order/applyService/getRecordInfoList',
    method: 'post',
    data
  })
}

// 获取全国省市区
export function fetchAddress(data) {
  return request({
    url: '/order/applyService/getAddress',
    method: 'post',
    data
  })
}

// 获取就医服务医院列表
export function fetchHospitalList(data) {
  return request({
    url: '/order/applyService/getHospitalInfo',
    method: 'post',
    data
  })
}

// 查看评价
export function fetchEvaluationInfo(data) {
  return request({
    url: '/order/applyService/getEvaluateInfo',
    method: 'post',
    data
  })
}
