import request from '@/utils/request'

// 解析openId
export function login(data) {
  return request({
    url: '/order/cardInfo/getOpenId',
    method: 'post',
    data
  })
}

// 获取直结卡信息
export function fetchCardInfo(data) {
  return request({
    url: '/order/cardInfo/getCardInfo',
    method: 'post',
    data
  })
}

// 绑定手机
export function bindPhone(data) {
  return request({
    url: '/order/cardInfo/comfirmPhone',
    method: 'post',
    data
  })
}

// 获取待还款列表
export function fetchUnPayList(data) {
  return request({
    url: '/order/cardInfo/getDebtList',
    method: 'post',
    data
  })
}

// 获取已还款列表
export function fetchPayList(data) {
  return request({
    url: '/order/cardInfo/getRepaymentInfo',
    method: 'post',
    data
  })
}

// 获取直结医院列表
export function fetchHospitalList(data) {
  return request({
    url: '/order/cardInfo/selectHospital',
    method: 'post',
    data
  })
}

// 获取医院详情
export function fetchHospitalDetail(data) {
  return request({
    url: '/order/cardInfo/selectHospitalDetail',
    method: 'post',
    data
  })
}

// 获取全国省市区
export function fetchCityList(data) {
  return request({
    url: '/order/cardInfo/getCityInfo',
    method: 'post',
    data
  })
}

// 获取付款链接
export function fetchRepaymentUrl(data) {
  return request({
    url: '/order/cardInfo/getRepaymentUrl',
    method: 'post',
    data
  })
}
