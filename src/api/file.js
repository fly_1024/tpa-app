import request from '@/utils/request'

export function uploadFiles(data) {
  return request({
    url: '/order/preauth/cliUploadFile',
    method: 'post',
    timeout: 1000 * 1000,
    data
  })
}

export function downLoadFileUrl(data) {
  return request({
    url: '/order/preauth/downLoadFileUrl',
    method: 'post',
    data
  })
}

export function removeFiles(key) {
  return request({
    url: `/order/applyService/deleteByKey/${key}`,
    method: 'get'
  })
}
