import request from '@/utils/request'

// 创建预授权
export function createPreInfo(data) {
  return request({
    url: '/order/preauth/addPreInfo',
    method: 'post',
    data
  })
}

// 获取就诊类型列表
export function fetchTreatTypes(data) {
  return request({
    url: '/order/preauth/getTreatType',
    method: 'post',
    data
  })
}

// 获取预授权记录
export function fetchPreRecord(data) {
  return request({
    url: '/order/preauth/getPreRecordPaging',
    method: 'post',
    data
  })
}

// 查看预授权详情
export function fetchPreDetail(data) {
  return request({
    url: '/order/preauth/getPreauthDetail',
    method: 'post',
    data
  })
}

// 取消预授权
export function cancalPre(data) {
  return request({
    url: '/order/preauth/cancalPreauth',
    method: 'post',
    data
  })
}

// 修改预授权信息
export function updatePre(data) {
  return request({
    url: '/order/preauth/updatePreauth',
    method: 'post',
    data
  })
}

// 获取预授权支持的医院
export function fetchPreHospital(data) {
  return request({
    url: '/order/preauth/getprehospital',
    method: 'post',
    data
  })
}
