import Mock from 'mockjs'

const count = 50
const hospitalList = []
const cityList = []
const recordList = []

for (let i = 0; i < count; i++) {
  hospitalList.push(Mock.mock({
    hospitalCode: '@integer(300, 5000)',
    chaddress: '@ctitle',
    hospitalName: '@ctitle'
  }))
}

for (let i = 0; i < 30; i++) {
  recordList.push(Mock.mock({
    orderCode: '@integer(300, 5000)',
    'serviceName|1': ['住院绿通', '专业门诊', '专家二诊'],
    applyTime: '@datetime("yyyy-MM-dd")',
    'detailStatus|1': ['01', '02', '03', '04', '05', '06', '07', '08'],
    insuredName: '@cname'
  }))
}

for (let i = 0; i < 200; i++) {
  cityList.push(Mock.mock({
    cityCode: '@integer(300, 5000)',
    deptDate: '@datetime("yyyy-MM-dd")',
    'cityInitial': /[A-Z]/,
    cityName: '@cname'
  }))
}

export default {
  login: () => {
    return {
      code: 200,
      success: true,
      msg: '成功',
      data: {
        openId: '123456'
      }
    }
  },
  getCardInfo: () => {
    return {
      code: 200,
      success: true,
      msg: '成功',
      data: Mock.mock({
        id: '@integer(300, 5000)',
        name: '@cname',
        birthday: '@datetime("yyyy-MM-dd")',
        'idType|1': ['01', '02', '03', '04'],
        idNo: '@integer(10000000000, 99999999999999)',
        customerNo: '@integer(1000, 9999)'
      })
    }
  },
  getRecordList: () => {
    return {
      code: 200,
      success: true,
      msg: '成功',
      data: recordList
    }
  },
  getHospitalList: (config) => {
    const { pageNum = 1, pageSize = 10 } = JSON.parse(config.body)
    const pageList = hospitalList.filter((item, index) => index < pageSize * pageNum && index >= pageSize * (pageNum - 1))

    return {
      code: 200,
      success: true,
      msg: '成功',
      total: hospitalList.length,
      rows: pageList
    }
  },
  getServiceDetail: () => {
    return {
      code: 200,
      success: true,
      msg: '成功',
      data: Mock.mock({
        id: '@integer(300, 5000)',
        name: '@cname',
        birthday: '@datetime("yyyy-MM-dd")',
        'idType|1': ['01', '02', '03', '04'],
        idNo: '@integer(10000000000, 99999999999999)',
        customerNo: '@integer(1000, 9999)',
        phone: '13888888888',
        inpatientArea: '@county(true)',
        inpatientHospital: '@ctitle',
        firstDept: '@ctitle',
        director: '@cname',
        symptomDescription: '@ctitle(30)'
      })
    }
  },
  getCityList: () => {
    return {
      code: 200,
      success: true,
      msg: '成功',
      data: cityList
    }
  },
  getServiceList: () => {
    return {
      code: 200,
      success: true,
      msg: '成功',
      data: [{
        serviceName: '住院绿通（普通）',
        serviceCode: '1111'
      }, {
        serviceName: '门诊绿通（普通）',
        serviceCode: '2222'
      }, {
        serviceName: '专家二诊（普通）',
        serviceCode: '3333'
      }]
    }
  },
  submitApplyInfo: () => {
    return {
      code: 200,
      success: true,
      msg: '成功',
      data: {}
    }
  },
  submitEvaluate: () => {
    return {
      code: 200,
      success: true,
      msg: '成功',
      data: {}
    }
  }
}
