import Mock from 'mockjs'

const count = 50
const hospitalList = []
const cityList = []
const unPayList = []

for (let i = 0; i < count; i++) {
  hospitalList.push(Mock.mock({
    hospitalCode: '@integer(300, 5000)',
    chaddress: '@ctitle',
    hospitalName: '@ctitle'
  }))
}

for (let i = 0; i < 3; i++) {
  unPayList.push(Mock.mock({
    rptNo: '@integer(300, 5000)',
    deptDate: '@datetime("yyyy-MM-dd")',
    'status|1': ['5', '1', '2', '3', '4'],
    insuredName: '@cname',
    'colStatus|1': ['01', '02'],
    deptAmount: '@integer(100, 50000)',
  }))
}

for (let i = 0; i < 200; i++) {
  cityList.push(Mock.mock({
    cityCode: '@integer(300, 5000)',
    deptDate: '@datetime("yyyy-MM-dd")',
    'cityInitial': /[A-Z]/,
    cityName: '@cname'
  }))
}

export default {
  login: (config) => {
    const { merchantCode, securityParam, securityKey } = JSON.parse(config.body)
    if (merchantCode && securityParam && securityKey) {
      return {
        code: 200,
        success: true,
        msg: '成功',
        data: {
          openId: '123456'
        }
      }
    }
    return {
      code: 500,
      success: false,
      msg: '解密失败',
      data: ''
    }
  },
  getCardInfo: () => {
    return {
      code: 200,
      success: true,
      msg: '成功',
      data: Mock.mock({
        id: '@integer(300, 5000)',
        idEnding: '@integer(1000, 9999)',
        effectiveDate: '@datetime("yyyy-MM-dd") 至 @datetime("yyyy-MM-dd")',
        'cardStatus|1': ['01', '02'],
        subPolicy: '@integer(1000, 9999)',
        name: '@cname',
        member: '@integer(1000, 9999)',
        befitCoverage: '住院直接结算',
        phone: '12345678902',
        currentTime: '@datetime("yyyy-MM-dd")'
      })
    }
  },
  getUnPayList: () => {
    return {
      code: 200,
      success: true,
      msg: '成功',
      data: unPayList
    }
  },
  getPayList: () => {
    return {
      code: 200,
      success: true,
      msg: '成功',
      data: unPayList
    }
  },
  getHospitalList: (config) => {
    const { pageNum = 1, pageSize = 10 } = JSON.parse(config.body)
    const pageList = hospitalList.filter((item, index) => index < pageSize * pageNum && index >= pageSize * (pageNum - 1))

    return {
      code: 200,
      success: true,
      msg: '成功',
      total: hospitalList.length,
      rows: pageList
    }
  },
  getHospitalDetail: () => {
    return {
      code: 200,
      success: true,
      msg: '成功',
      data: Mock.mock({
        id: '@integer(300, 5000)',
        worktime: '@datetime("yyyy-MM-dd") 至 @datetime("yyyy-MM-dd")',
        'leadFlag|2-4': ['公立', '三甲', '驻点服务', '特定医院'],
        hospitalName: '@cname',
        chaddreess: '@ctitle',
        networkHospitalType: '@ctitle',
        introduction: '@title',
        remark: '@title',
        sellingpoint: '@title'
      })
    }
  },
  getCityList: () => {
    return {
      code: 200,
      success: true,
      msg: '成功',
      data: cityList
    }
  }
}
