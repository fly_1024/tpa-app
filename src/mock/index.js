import Mock from 'mockjs'

import cardInfoApi from './cardInfo'
import applyServiceApi from './applyService'

Mock.setup({
  timeout: '350-600'
})

// 匹配需要mock数据的接口

Mock.mock(RegExp('/order/cardInfo/getOpenId'), 'post', cardInfoApi.login)
Mock.mock(RegExp('/order/cardInfo/getCardInfo'), 'post', cardInfoApi.getCardInfo)
Mock.mock(RegExp('/order/cardInfo/getDebtList'), 'post', cardInfoApi.getUnPayList)
Mock.mock(RegExp('/order/cardInfo/getRepaymentInfo'), 'post', cardInfoApi.getPayList)
Mock.mock(RegExp('/order/cardInfo/selectHospital'), 'post', cardInfoApi.getHospitalList)
Mock.mock(RegExp('/order/cardInfo/selectHospitalDetail'), 'post', cardInfoApi.getHospitalDetail)
Mock.mock(RegExp('/order/cardInfo/getCityInfo'), 'post', cardInfoApi.getCityList)
Mock.mock(RegExp('/order/cardInfo/comfirmPhone'), 'post', cardInfoApi.login)

Mock.mock(RegExp('/order/applyService/getCustomerInfo'), 'post', applyServiceApi.getCardInfo)
Mock.mock(RegExp('/order/applyService/getApplyInfo'), 'post', applyServiceApi.submitApplyInfo)
Mock.mock(RegExp('/order/applyService/getServiceDetailInfo'), 'post', applyServiceApi.getServiceDetail)
Mock.mock(RegExp('/order/applyService/getRecordInfoList'), 'post', applyServiceApi.getRecordList)
Mock.mock(RegExp('/order/applyService/submitEvaluate'), 'post', applyServiceApi.submitEvaluate)
Mock.mock(RegExp('/order/applyService/getHospitalInfo'), 'post', cardInfoApi.getHospitalList)
Mock.mock(RegExp('/order/applyService/getServiceInfo'), 'post', applyServiceApi.getServiceList)

export default Mock
