import router from './router'
import store from './store'
import { getToken, setToken } from '@/utils/auth' // get token from cookie
const whiteList = ['/401'] // no redirect whitelist

router.beforeEach(async(to, from, next) => {
  const { openId } = to.query
  if (openId) {
    store.commit('SET_TOKEN', openId)
    setToken(openId)
  }
  // determine whether the user has logged in
  const hasToken = getToken()
  if (hasToken) {
    const hasUserInfo = store.state.userInfo.insuredNo
    if (hasUserInfo) {
      next()
    } else {
      await store.dispatch('getUserInfo')
      next()
    }
  } else {
    if (whiteList.indexOf(to.path) !== -1) {
      next()
    } else {
      try {
        const { merchantCode, securityParam, securityKey } = to.query
        await store.dispatch('login', { merchantCode, securityParam, securityKey })
        await store.dispatch('getUserInfo')
        next()
      } catch (error) {
        next('/401')
      }
    }
  }
})
