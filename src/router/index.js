import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/debit-card',
    name: 'DebitCard',
    component: () => import('../views/debit-card/index.vue')
  },
  {
    path: '/un-pay',
    name: 'unPay',
    component: () => import('../views/debit-card/un-pay')
  },
  {
    path: '/my-pay',
    name: 'myPay',
    component: () => import('../views/debit-card/my-pay')
  },
  {
    path: '/pay-notice',
    name: 'payNotice',
    component: () => import('../views/debit-card/pay-notice')
  },
  {
    path: '/hospital-search',
    name: 'HospitalSearch',
    component: () => import('../views/debit-card/hospital-search')
  },
  {
    path: '/hospital-details',
    name: 'HospitalDetails',
    component: () => import('../views/debit-card/hospital-details')
  },
  {
    path: '/city-select',
    name: 'CitySelect',
    component: () => import('../views/debit-card/city-select')
  },
  {
    path: '/medical-services',
    name: 'MedicalServices',
    component: () => import('../views/medical-services/index.vue')
  },
  {
    path: '/service-application',
    name: 'ServiceApplication',
    component: () => import('../views/medical-services/service-application')
  },
  {
    path: '/service-view',
    name: 'ServiceView',
    component: () => import('../views/medical-services/service-view')
  },
  {
    path: '/service-record',
    name: 'ServiceRecord',
    component: () => import('../views/medical-services/service-record')
  },
  {
    path: '/evaluate',
    name: 'Evaluate',
    component: () => import('../views/medical-services/evaluate')
  },
  {
    path: '/evaluate-view',
    name: 'EvaluateView',
    component: () => import('../views/medical-services/evaluate-view')
  },
  {
    path: '/authorization',
    name: 'Authorization',
    component: () => import('../views/authorization/index')
  },
  {
    path: '/authorization-apply',
    name: 'AuthorizationApply',
    component: () => import('../views/authorization/authorization-apply')
  },
  {
    path: '/authorization-details',
    name: 'AuthorizationDetails',
    component: () => import('../views/authorization/authorization-details')
  },
  {
    path: '/authorization-record',
    name: 'AuthorizationRecord',
    component: () => import('../views/authorization/authorization-record')
  },
  {
    path: '/authorization-hospital',
    name: 'AuthorizationHospital',
    component: () => import('../views/authorization/hospital-search')
  },
  {
    path: '/authorization-city',
    name: 'AuthorizationCity',
    component: () => import('../views/authorization/city-select')
  },
  {
    path: '/401',
    name: '401',
    component: () => import('../views/401')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
