import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import { login, fetchCardInfo } from '@/api/cardInfo'
import { fetchCustomerInfo } from '@/api/applyService'
import { getToken, setToken, removeToken } from '@/utils/auth'
import router, { resetRouter } from '@/router'
import { Notify } from 'vant'

export default new Vuex.Store({
  state: {
    token: getToken(),
    userInfo: {},
    cachedViews: []
  },
  getters: {
    token: state => state.token
  },
  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_USER_INFO: (state, info) => {
      state.userInfo = info
    },
    SET_CACHED: (state, cachedView) => {
      !state.cachedViews.includes(cachedView) &&
      state.cachedViews.push(cachedView)
    },
    DEL_CACHED: (state, cachedView) => {
      state.cachedViews = state.cachedViews.filter(item => item !== cachedView)
    },
  },
  actions: {
    // user login
    login({ commit }, userInfo) {
      const { merchantCode, securityParam, securityKey } = userInfo
      return new Promise((resolve, reject) => {
        // const openId = '473E9D82292547B6BCC17EBF4E69DFID'
        // commit('SET_TOKEN', openId)
        // setToken(openId)
        // resolve(openId)
        login({ merchantCode, securityParam, securityKey }).then(res => {
          const { data } = res
          commit('SET_TOKEN', data.openId)
          setToken(data.openId)
          resolve(data.openId)
        }).catch(error => {
          reject(error)
        })
      })
    },

    getUserInfo({ commit, state }) {
      return new Promise((resolve, reject) => {
        fetchCustomerInfo({ openId: state.token }).then(res => {
          const { data } = res

          if (!data) {
            reject('Verification failed, please Login again.')
          }

          commit('SET_USER_INFO', data)
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // remove token
    resetToken({ commit }) {
      return new Promise(resolve => {
        commit('SET_TOKEN', '')
        removeToken()
        resolve()
      })
    }
  },
  modules: {
  }
})
