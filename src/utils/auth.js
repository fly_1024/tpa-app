import sha256 from 'crypto-js/sha256'
const TokenKey = 'token'

export function getToken() {
  return sessionStorage.getItem(TokenKey)
}

export function setToken(token) {
  return sessionStorage.setItem(TokenKey, token)
}

export function removeToken() {
  return sessionStorage.removeItem(TokenKey)
}

/**
 * 签名
 * 签名格式：str + timestamp + token + data(字典升序)
 * @param method 请求方式
 * @param data 签名数据
 */
export const sign = (method, data = {}) => {
  // timestamp 签名时间戳
  const timestamp = +new Date()
  // Token令牌
  const token = getToken()
  const ret = JSON.stringify(data)
  let signStr = ''
  if (token) {
    signStr = sha256(timestamp + token + ret)
  } else {
    signStr = sha256(timestamp + ret)
  }
  return {
    timestamp: timestamp,
    sign: signStr
  }
}
