import axios from 'axios'
import { Notify } from 'vant'
import store from '@/store'
import { getToken, sign } from '@/utils/auth'

// 请求签名
function handleSign(config) {
  const signObj = sign(config.method, config.data)
  config.headers = Object.assign({}, config.headers, signObj)
  return config
}

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API,
  timeout: 60000,
  headers: {
    'content-type': 'application/json'
  }
})

// request interceptor
service.interceptors.request.use(
  config => {
    const token = store.getters.token
    // const token = '57366360-44a9-4b59-a580-531464c8ee20'
    if (config.method === 'post') {
      config = handleSign(config)
    }
    if (token) {
      // 请求头设置Token
      config.headers['Authorization'] = 'Bearer ' + token
    }
    return config
  },
  error => {
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
  */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    const res = response.data

    // if the custom code is not 20000, it is judged as an error.
    if (res.code !== 200) {
      Notify({ type: 'danger', message: res.data || res.msg || 'Error' })

      if (res.code === 403) {
        // to re-login
        store.dispatch('user/resetToken').then(() => {
          location.reload()
        })
      }
      return Promise.reject(new Error(res.data || res.msg || 'Error'))
    } else {
      return res
    }
  },
  error => {
    console.log('err' + error) // for debug
    Notify({ type: 'danger', message: error.message || 'Error' })
    return Promise.reject(error)
  }
)

export default service
